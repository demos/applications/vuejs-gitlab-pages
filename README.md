# vuejs-gitlab-pages

This is a proof of concept project on how to host a VueJs application from scratch

## Prequistie

- Node.js (>=6.x, 8.x preferred)
- npm version 3+
- Git

## Getting started

1. First install vue-cli
```bash
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```
You can check you haev the right version of Vue with
```bash
vue --version
```

2. Create your application using 
```bash
vue create name-of-app
```